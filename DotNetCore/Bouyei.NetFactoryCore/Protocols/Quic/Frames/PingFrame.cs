﻿using Bouyei.NetFactoryCore.Protocols.Quic.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bouyei.NetFactoryCore.Protocols.Quic.Frames
{
    public class PingFrame : Frame
    {
        public override byte Type => 0x01;

        public override void Decode(ByteArray array)
        {
            byte type = array.ReadByte();
        }

        public override byte[] Encode()
        {
            return new byte[] { Type };

            //List<byte> data = new List<byte>();
            //data.Add(Type);

            //return data.ToArray();
        }
    }
}
